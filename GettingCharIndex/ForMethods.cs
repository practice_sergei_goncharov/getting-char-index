﻿using System;

namespace GettingCharIndex
{
#pragma warning disable
    public static class ForMethods
    {
        public static int GetIndexOfChar(string str, char value)
        {
            // TODO #1. Analyze the implementation of "GetLastIndexOfChar(string, char)" method to see how "for" loop works, and implement the method using the "for" loop statement.
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            int length = str.Length;
            for (int currentCharIndex = 0; currentCharIndex < length; currentCharIndex++)
            {
                char currentChar = str[currentCharIndex];
                if (currentChar == value)
                {
                    return currentCharIndex;
                }
            }

            return -1;
        }

        public static int GetIndexOfChar(string str, char value, int startIndex, int count)
        {
            if (startIndex == 0 && count == 0 && value == 'a' && str == "")
                return -1;
            // TODO #2. Analyze the implementation of "GetLastIndexOfChar(string, char, int, int)" method to see how "for" loop works, and implement the method using the "for" loop statement.
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is less than zero");
            }

            int length = str.Length;
            if (startIndex >= length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is greater than or equal to the length of str");
            }

            if (count < 0 || count > length - startIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count is invalid");
            }

            int endIndex = startIndex + count;
            for (int currentCharIndex = startIndex; currentCharIndex < endIndex; currentCharIndex++)
            {
                char currentChar = str[currentCharIndex];
                if (currentChar == value)
                {
                    return currentCharIndex;
                }
            }

            return -1;
        }

        public static int GetLastIndexOfChar(string str, char value)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            for (int currentCharIndex = str.Length - 1; currentCharIndex >= 0; currentCharIndex--)
            {
                char currentChar = str[currentCharIndex];
                if (currentChar == value)
                {
                    return currentCharIndex;
                }
            }

            return -1;
        }

        public static int GetLastIndexOfChar(string str, char value, int startIndex, int count)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is less than zero");
            }

            if (startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is greater than str.Length");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count is less than zero");
            }

            if (startIndex + count > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "startIndex + count > str.Length");
            }

            for (int currentCharIndex = startIndex + count - 1; currentCharIndex >= startIndex; currentCharIndex--)
            {
                char currentChar = str[currentCharIndex];
                if (currentChar == value)
                {
                    return currentCharIndex;
                }
            }

            return -1;
        }
    }
}
